﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Filtery.Utils
{
    public static class Filters
    {
        public static Bitmap Invert(Image image)
        {
            Bitmap originalBmp = new Bitmap(image);
            Bitmap newBmp = new Bitmap(originalBmp.Width, originalBmp.Height);
            Color newColor = new Color();
            Color originalColor = new Color();

            for (int x = 0; x < originalBmp.Width; x++)
            {
                for (int y = 0; y < originalBmp.Height; y++)
                {
                    originalColor = originalBmp.GetPixel(x, y);

                    newColor = Color.FromArgb(255 - originalColor.R,
                                              255 - originalColor.G,
                                              255 - originalColor.B);
                    newBmp.SetPixel(x, y, newColor);
                }
            }

            return newBmp;
        }

        public static Bitmap Grayscale(Image image)
        {

            Bitmap originalBmp = new Bitmap(image);
            Bitmap newBmp = new Bitmap(originalBmp.Width, originalBmp.Height);
            Color newColor = new Color();
            Color originalColor = new Color();

            float grayValue = 0.0f;

            for (int x = 0; x < originalBmp.Width; x++)
            {
                for (int y = 0; y < originalBmp.Height; y++)
                {
                    originalColor = originalBmp.GetPixel(x, y);

                    grayValue = originalColor.R * 0.299f + originalColor.G * 0.587f + originalColor.B * 0.114f;
                    
                    newColor = Color.FromArgb((int)grayValue, (int)grayValue, (int)grayValue);

                    newBmp.SetPixel(x, y, newColor);
                }
            }

            return newBmp;
        }

        public static Bitmap Pixelate(Image image)
        {
            Bitmap originalBmp = new Bitmap(image);
            Bitmap newBmp = new Bitmap(originalBmp.Width, originalBmp.Height);
            Color newColor = new Color();
            Color originalColor = new Color();

            int pixel = 16;

            int redsum = 0;
            int greensum = 0;
            int bluesum = 0;

            int r = 0;
            int g = 0;
            int b = 0;

            for (int x = 0; x < originalBmp.Width - pixel; x += pixel)
            {
                for (int y = 0; y < originalBmp.Height - pixel; y += pixel)
                {
                    redsum = greensum = bluesum = 0;

                    for (int xpixel = x; xpixel < (x + pixel); xpixel++)
                    {
                        for (int ypixel = y; ypixel < (y + pixel); ypixel++)
                        {
                            originalColor = originalBmp.GetPixel(xpixel, ypixel);
                            redsum += originalColor.R;
                            greensum += originalColor.G;
                            bluesum += originalColor.B;
                        }
                    }

                    r = redsum / (pixel * pixel);
                    g = greensum / (pixel * pixel);
                    b = bluesum / (pixel * pixel);

                    newColor = Color.FromArgb(r, g, b);

                    for (int xpixel = x; xpixel < (x + pixel); xpixel++)
                    {
                        for (int ypixel = y; ypixel < (y + pixel); ypixel++)
                        {
                           newBmp.SetPixel(xpixel, ypixel, newColor);
                        }
                    }
                }
             }

            return newBmp;
        }

        public static Bitmap Binary(Image image)
        {
            Bitmap originalBmp = new Bitmap(image);
            Bitmap newBmp = new Bitmap(originalBmp.Width, originalBmp.Height);
            Color newColor = new Color();
            Color originalColor = new Color();

            int grayValue = 0;

            for (int x = 0; x < originalBmp.Width; x++)
            {
                for (int y = 0; y < originalBmp.Height; y++)
                {
                    originalColor = originalBmp.GetPixel(x, y);

                    grayValue = (originalColor.R + originalColor.G + originalColor.B) / 3;

                    if(grayValue > 128)
                    {
                        grayValue = 255;
                    } 
                    else
                    {
                        grayValue = 0;
                    }

                    newColor = Color.FromArgb(grayValue, grayValue, grayValue);

                    newBmp.SetPixel(x, y, newColor);
                }
            }

            return newBmp;
        }

        public static Bitmap ColorGradient(Image image)
        {
            Bitmap originalBmp = new Bitmap(image);
            Bitmap newBmp = new Bitmap(originalBmp.Width, originalBmp.Height);
            Color originalColor = new Color();

            float initialRed = 255;
            float initialGreen = 94;
            float initialBlue = 94;

            float finishingRed = 113;
            float finishingGreen = 130;
            float finishingBlue = 255;

            int red = 0;
            int green = 0;
            int blue = 0;

            float deltaRed = (finishingRed - initialRed) / originalBmp.Width;
            float deltaGreen = (finishingGreen - initialGreen) / originalBmp.Width;
            float deltaBlue = (finishingBlue - initialBlue) / originalBmp.Width;

            for (int x = 0; x < originalBmp.Width; x++)
            {
                for (int y = 0; y < originalBmp.Height; y++)
                {
                    originalColor = originalBmp.GetPixel(x, y);

                    red = (int)((initialRed / 255.0f) * originalColor.R);
                    green = (int)((initialGreen / 255.0f) * originalColor.G);
                    blue = (int)((initialBlue / 255.0f) * originalColor.B);

                    if (red > 255) red = 255;
                    else if(red < 0) red = 0;

                    if(green > 255) green = 255;
                    else if(green < 0) green = 0;

                    if(blue > 255) blue = 255;
                    else if(blue < 0) blue = 0;

                    newBmp.SetPixel(x, y, Color.FromArgb(red, green, blue));

                }

                initialRed += deltaRed;
                initialGreen += deltaGreen;
                initialBlue += deltaBlue;

            }

            return newBmp;
        }
    }

    public static class VideoFilters
    {
     
        public static Bitmap Invert(Bitmap frame)
        {
            try
            {
                Bitmap bmp = (Bitmap)frame.Clone();

                unsafe
                {
                    // Lock the bitmap's bits.  
                    Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
                    BitmapData bmpData = bmp.LockBits(rect, ImageLockMode.ReadWrite, bmp.PixelFormat);

                    // Define variables for bytes per pixel, as well as Image Width & Height
                    int bytesPerPixel = Image.GetPixelFormatSize(bmp.PixelFormat) / 8;
                    int heightInPixels = bmpData.Height;
                    int widthInBytes = bmpData.Width * bytesPerPixel;

                    // Define a pointer to the first pixel in the locked image
                    // Scan0 gets or sets the address of the first pixel data in the bitmap.
                    // This can also be thought of as the first scan line in the bitmap.
                    byte* PtrFirstPixel = (byte*)bmpData.Scan0;

                    Parallel.For(0, heightInPixels, y =>
                    {
                        byte* currentLine = PtrFirstPixel + (y * bmpData.Stride);
                        for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                        {
                            // GET: each pixel color (R, G, & B)
                            int oldBlue = currentLine[x];
                            int oldGreen = currentLine[x + 1];
                            int oldRed = currentLine[x + 2];

                            int newBlue = 255 - oldBlue;
                            int newGreen = 255 - oldGreen;
                            int newRed = 255 - oldRed;


                            // SET: kero out the Blue, copy Green and Red unchanged
                            currentLine[x] = (byte)newBlue;
                            currentLine[x + 1] = (byte)newGreen;
                            currentLine[x + 2] = (byte)newRed;
                        }
                    });

                    bmp.UnlockBits(bmpData);
                }

                return bmp;
            }
            catch (Exception ex)
            {

                MessageBox.Show("Ha ocurrido un error en el procesado del video \n Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                System.Windows.Forms.Application.Exit();
                return frame;
            }

           
        }

        public static Bitmap Grayscale(Bitmap frame)
        {
            try
            {
                Bitmap bmp = (Bitmap)frame.Clone();

                unsafe
                {
                    // Lock the bitmap's bits.  
                    Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
                    BitmapData bmpData = bmp.LockBits(rect, ImageLockMode.ReadWrite, bmp.PixelFormat);

                    // Define variables for bytes per pixel, as well as Image Width & Height
                    int bytesPerPixel = Image.GetPixelFormatSize(bmp.PixelFormat) / 8;
                    int heightInPixels = bmpData.Height;
                    int widthInBytes = bmpData.Width * bytesPerPixel;

                    // Define a pointer to the first pixel in the locked image
                    // Scan0 gets or sets the address of the first pixel data in the bitmap.
                    // This can also be thought of as the first scan line in the bitmap.
                    byte* PtrFirstPixel = (byte*)bmpData.Scan0;

                    Parallel.For(0, heightInPixels, y =>
                    {
                        byte* currentLine = PtrFirstPixel + (y * bmpData.Stride);
                        for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                        {
                            // GET: each pixel color (R, G, & B)
                            int oldBlue = currentLine[x];
                            int oldGreen = currentLine[x + 1];
                            int oldRed = currentLine[x + 2];

                            int grayValue = (byte)(0.299f * oldRed + 0.587f * oldGreen + 0.114f * oldBlue);


                            // SET: kero out the Blue, copy Green and Red unchanged
                            currentLine[x] = (byte)grayValue;
                            currentLine[x + 1] = (byte)grayValue;
                            currentLine[x + 2] = (byte)grayValue;
                        }
                    });

                    bmp.UnlockBits(bmpData);
                }

                return bmp;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ha ocurrido un error en el procesado del video \n Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                System.Windows.Forms.Application.Exit();
                return frame;
            }

           
        }

        public static Bitmap Sepia(Bitmap frame)
        {
            try
            {
                Bitmap bmp = (Bitmap)frame.Clone();

                unsafe
                {
                    // Lock the bitmap's bits.  
                    Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
                    BitmapData bmpData = bmp.LockBits(rect, ImageLockMode.ReadWrite, bmp.PixelFormat);

                    // Define variables for bytes per pixel, as well as Image Width & Height
                    int bytesPerPixel = Image.GetPixelFormatSize(bmp.PixelFormat) / 8;
                    int heightInPixels = bmpData.Height;
                    int widthInBytes = bmpData.Width * bytesPerPixel;

                    // Define a pointer to the first pixel in the locked image
                    // Scan0 gets or sets the address of the first pixel data in the bitmap.
                    // This can also be thought of as the first scan line in the bitmap.
                    byte* PtrFirstPixel = (byte*)bmpData.Scan0;

                    Parallel.For(0, heightInPixels, y =>
                    {
                        byte* currentLine = PtrFirstPixel + (y * bmpData.Stride);
                        for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                        {
                            // GET: each pixel color (R, G, & B)
                            int oldBlue = currentLine[x];
                            int oldGreen = currentLine[x + 1];
                            int oldRed = currentLine[x + 2];

                            float newBlue = (float)(oldBlue * 0.131f + oldGreen * 0.534f + oldRed * 0.272f);
                            float newGreen = (float)(oldBlue * 0.168f + oldGreen * 0.686f + oldRed * 0.349f);
                            float newRed = (float)(oldBlue * 0.189f + oldGreen * 0.769f + oldRed * 0.393f);

                            if (newBlue > 255) newBlue = 255;
                            if (newGreen > 255) newGreen = 255;
                            if (newRed > 255) newRed = 255;


                            // SET: kero out the Blue, copy Green and Red unchanged
                            currentLine[x] = (byte)newBlue;
                            currentLine[x + 1] = (byte)newGreen;
                            currentLine[x + 2] = (byte)newRed;
                        }
                    });

                    bmp.UnlockBits(bmpData);
                }

                return bmp;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ha ocurrido un error en el procesado del video \n Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                System.Windows.Forms.Application.Exit();
                return frame;
            }

            

            
        }

        public static Bitmap Binary(Bitmap frame)
        {
            try
            {
                Bitmap bmp = (Bitmap)frame.Clone();

                unsafe
                {
                    // Lock the bitmap's bits.  
                    Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
                    BitmapData bmpData = bmp.LockBits(rect, ImageLockMode.ReadWrite, bmp.PixelFormat);

                    // Define variables for bytes per pixel, as well as Image Width & Height
                    int bytesPerPixel = Image.GetPixelFormatSize(bmp.PixelFormat) / 8;
                    int heightInPixels = bmpData.Height;
                    int widthInBytes = bmpData.Width * bytesPerPixel;

                    // Define a pointer to the first pixel in the locked image
                    // Scan0 gets or sets the address of the first pixel data in the bitmap.
                    // This can also be thought of as the first scan line in the bitmap.
                    byte* PtrFirstPixel = (byte*)bmpData.Scan0;

                    Parallel.For(0, heightInPixels, y =>
                    {
                        byte* currentLine = PtrFirstPixel + (y * bmpData.Stride);
                        for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                        {
                            // GET: each pixel color (R, G, & B)
                            int oldBlue = currentLine[x];
                            int oldGreen = currentLine[x + 1];
                            int oldRed = currentLine[x + 2];

                            int grayValue = (byte)((oldRed + oldGreen + oldBlue) / 3);

                            if (grayValue > 128)
                            {
                                grayValue = 255;
                            }
                            else
                            {
                                grayValue = 0;
                            }

                            // SET: kero out the Blue, copy Green and Red unchanged
                            currentLine[x] = (byte)grayValue;
                            currentLine[x + 1] = (byte)grayValue;
                            currentLine[x + 2] = (byte)grayValue;

                        }
                    });

                    bmp.UnlockBits(bmpData);
                }

                return bmp;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ha ocurrido un error en el procesado del video \n Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                System.Windows.Forms.Application.Exit();
                return frame;

            }
            
            
        }

        public static Bitmap Matrix(Bitmap frame)
        {
            try
            {
                Bitmap bmp = (Bitmap)frame.Clone();

                unsafe
                {
                    // Lock the bitmap's bits.  
                    Rectangle rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
                    BitmapData bmpData = bmp.LockBits(rect, ImageLockMode.ReadWrite, bmp.PixelFormat);

                    // Define variables for bytes per pixel, as well as Image Width & Height
                    int bytesPerPixel = Image.GetPixelFormatSize(bmp.PixelFormat) / 8;
                    int heightInPixels = bmpData.Height;
                    int widthInBytes = bmpData.Width * bytesPerPixel;

                    // Define a pointer to the first pixel in the locked image
                    // Scan0 gets or sets the address of the first pixel data in the bitmap.
                    // This can also be thought of as the first scan line in the bitmap.
                    byte* PtrFirstPixel = (byte*)bmpData.Scan0;

                    Parallel.For(0, heightInPixels, y =>
                    {
                        byte* currentLine = PtrFirstPixel + (y * bmpData.Stride);
                        for (int x = 0; x < widthInBytes; x = x + bytesPerPixel)
                        {
                            // GET: each pixel color (R, G, & B)
                            int oldBlue = currentLine[x];
                            int oldGreen = currentLine[x + 1];
                            int oldRed = currentLine[x + 2];

                            int grayValue = (byte)((oldRed + oldGreen + oldBlue) / 3);

                            if (grayValue > 128)
                            {
                                grayValue = 255;
                            }
                            else
                            {
                                grayValue = 0;
                            }

                            // SET: kero out the Blue, copy Green and Red unchanged
                            currentLine[x] = (byte)0;
                            currentLine[x + 1] = (byte)grayValue;
                            currentLine[x + 2] = (byte)0;
                        }
                    });

                    bmp.UnlockBits(bmpData);
                }

                return bmp;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ha ocurrido un error en el procesado del video \n Error: " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                System.Windows.Forms.Application.Exit();
                return frame;
            }

           
 
        }   
    }
}
