﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Filtery.Classes
{
    internal static class AppColors
    {
        public static Color textColor = Color.White;
        public static Color backgroundColor = Color.FromArgb(31, 0, 33);
        public static Color sideMenuColor = Color.FromArgb(45, 3, 29);
        public static Color activeControlColor = Color.FromArgb(74, 8, 20);
        public static Color lighterActiveControlColor = Color.FromArgb(117, 16, 6);
    }
}
