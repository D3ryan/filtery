﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using FontAwesome.Sharp;
using Filtery.Classes;

namespace Filtery
{
    public partial class AppForm : Form
    {
        private IconButton currentButton;
        private Panel leftBorderButton;
        private Form currentChildForm;

        public AppForm()
        {
            InitializeComponent();
            leftBorderButton = new Panel();
            leftBorderButton.Size = new Size(7, 60);
            panelMenu.Controls.Add(leftBorderButton);   
        }

        private void ActivateButton(object senderButton)
        {
            if(senderButton != null)
            {
                DisableButton();
                currentButton = (IconButton) senderButton;
                currentButton.BackColor = AppColors.activeControlColor;
                currentButton.TextAlign = ContentAlignment.MiddleCenter;
                currentButton.ImageAlign = ContentAlignment.MiddleCenter;

                leftBorderButton.BackColor = AppColors.lighterActiveControlColor;
                leftBorderButton.Location = new Point(0, currentButton.Location.Y);
                leftBorderButton.Visible = true;
                leftBorderButton.BringToFront();

                iconCurrentChildForm.IconChar = currentButton.IconChar;
            }
        }

        private void DisableButton()
        {
            if(currentButton != null)
            {
                currentButton.BackColor = AppColors.sideMenuColor;
                currentButton.TextAlign = ContentAlignment.MiddleLeft;
                currentButton.ImageAlign = ContentAlignment.MiddleLeft;
            }
        }

        private void OpenChildForm(Form childForm)
        {
            if (currentChildForm != null)
                currentChildForm.Close();

            currentChildForm = childForm;
            childForm.TopLevel = false;
            childForm.FormBorderStyle = FormBorderStyle.None;
            childForm.Dock = DockStyle.Fill;
            panelChildForm.Controls.Add(childForm);
            panelChildForm.Tag = childForm;
            childForm.BringToFront();
            childForm.Show();
            labelTitleChildForm.Text = childForm.Text;

        }

        private void ImagesButton_Click(object sender, EventArgs e)
        {
            ActivateButton(sender);
            OpenChildForm(new ImagesForm());
        }

        private void CameraButton_Click(object sender, EventArgs e)
        {
            ActivateButton(sender);
            OpenChildForm(new CameraForm());
        }
        private void VideosButton_Click(object sender, EventArgs e)
        {
            ActivateButton(sender);
            OpenChildForm(new VideosForm());
        }

        private void buttonHome_Click(object sender, EventArgs e)
        {
            currentChildForm.Close();
            Reset();
        }

        private void Reset()
        {
            DisableButton();
            leftBorderButton.Visible = false;
            iconCurrentChildForm.IconChar = IconChar.Home;
            labelTitleChildForm.Text = "Home";
        }

        private void AppForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (currentChildForm != null)
                currentChildForm.Close();
        }
    }
}
