﻿namespace Filtery
{
    partial class AppForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AppForm));
            this.panelMenu = new System.Windows.Forms.Panel();
            this.VideosButton = new FontAwesome.Sharp.IconButton();
            this.CameraButton = new FontAwesome.Sharp.IconButton();
            this.ImagesButton = new FontAwesome.Sharp.IconButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonHome = new System.Windows.Forms.PictureBox();
            this.panelTitleBar = new System.Windows.Forms.Panel();
            this.labelTitleChildForm = new System.Windows.Forms.Label();
            this.iconCurrentChildForm = new FontAwesome.Sharp.IconPictureBox();
            this.panelChildForm = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelMenu.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonHome)).BeginInit();
            this.panelTitleBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconCurrentChildForm)).BeginInit();
            this.panelChildForm.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelMenu
            // 
            this.panelMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(3)))), ((int)(((byte)(29)))));
            this.panelMenu.Controls.Add(this.VideosButton);
            this.panelMenu.Controls.Add(this.CameraButton);
            this.panelMenu.Controls.Add(this.ImagesButton);
            this.panelMenu.Controls.Add(this.panel1);
            this.panelMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelMenu.Location = new System.Drawing.Point(0, 0);
            this.panelMenu.Name = "panelMenu";
            this.panelMenu.Size = new System.Drawing.Size(220, 690);
            this.panelMenu.TabIndex = 0;
            // 
            // VideosButton
            // 
            this.VideosButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.VideosButton.FlatAppearance.BorderSize = 0;
            this.VideosButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.VideosButton.ForeColor = System.Drawing.Color.White;
            this.VideosButton.IconChar = FontAwesome.Sharp.IconChar.Video;
            this.VideosButton.IconColor = System.Drawing.Color.White;
            this.VideosButton.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.VideosButton.IconSize = 32;
            this.VideosButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.VideosButton.Location = new System.Drawing.Point(0, 306);
            this.VideosButton.Name = "VideosButton";
            this.VideosButton.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.VideosButton.Size = new System.Drawing.Size(220, 60);
            this.VideosButton.TabIndex = 2;
            this.VideosButton.Text = "Videos";
            this.VideosButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.VideosButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.VideosButton.UseVisualStyleBackColor = true;
            this.VideosButton.Click += new System.EventHandler(this.VideosButton_Click);
            // 
            // CameraButton
            // 
            this.CameraButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.CameraButton.FlatAppearance.BorderSize = 0;
            this.CameraButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CameraButton.ForeColor = System.Drawing.Color.White;
            this.CameraButton.IconChar = FontAwesome.Sharp.IconChar.Camera;
            this.CameraButton.IconColor = System.Drawing.Color.White;
            this.CameraButton.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.CameraButton.IconSize = 32;
            this.CameraButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CameraButton.Location = new System.Drawing.Point(0, 246);
            this.CameraButton.Name = "CameraButton";
            this.CameraButton.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.CameraButton.Size = new System.Drawing.Size(220, 60);
            this.CameraButton.TabIndex = 1;
            this.CameraButton.Text = "Camera";
            this.CameraButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CameraButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.CameraButton.UseVisualStyleBackColor = true;
            this.CameraButton.Click += new System.EventHandler(this.CameraButton_Click);
            // 
            // ImagesButton
            // 
            this.ImagesButton.Dock = System.Windows.Forms.DockStyle.Top;
            this.ImagesButton.FlatAppearance.BorderSize = 0;
            this.ImagesButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ImagesButton.ForeColor = System.Drawing.Color.White;
            this.ImagesButton.IconChar = FontAwesome.Sharp.IconChar.Image;
            this.ImagesButton.IconColor = System.Drawing.Color.White;
            this.ImagesButton.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.ImagesButton.IconSize = 32;
            this.ImagesButton.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ImagesButton.Location = new System.Drawing.Point(0, 186);
            this.ImagesButton.Name = "ImagesButton";
            this.ImagesButton.Padding = new System.Windows.Forms.Padding(10, 0, 20, 0);
            this.ImagesButton.Size = new System.Drawing.Size(220, 60);
            this.ImagesButton.TabIndex = 0;
            this.ImagesButton.Text = "Images";
            this.ImagesButton.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ImagesButton.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.ImagesButton.UseVisualStyleBackColor = true;
            this.ImagesButton.Click += new System.EventHandler(this.ImagesButton_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonHome);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(220, 186);
            this.panel1.TabIndex = 0;
            // 
            // buttonHome
            // 
            this.buttonHome.Image = ((System.Drawing.Image)(resources.GetObject("buttonHome.Image")));
            this.buttonHome.Location = new System.Drawing.Point(48, 35);
            this.buttonHome.Name = "buttonHome";
            this.buttonHome.Size = new System.Drawing.Size(114, 122);
            this.buttonHome.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.buttonHome.TabIndex = 0;
            this.buttonHome.TabStop = false;
            this.buttonHome.Click += new System.EventHandler(this.buttonHome_Click);
            // 
            // panelTitleBar
            // 
            this.panelTitleBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(3)))), ((int)(((byte)(29)))));
            this.panelTitleBar.Controls.Add(this.labelTitleChildForm);
            this.panelTitleBar.Controls.Add(this.iconCurrentChildForm);
            this.panelTitleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitleBar.Location = new System.Drawing.Point(220, 0);
            this.panelTitleBar.Name = "panelTitleBar";
            this.panelTitleBar.Size = new System.Drawing.Size(768, 75);
            this.panelTitleBar.TabIndex = 1;
            // 
            // labelTitleChildForm
            // 
            this.labelTitleChildForm.AutoSize = true;
            this.labelTitleChildForm.ForeColor = System.Drawing.Color.White;
            this.labelTitleChildForm.Location = new System.Drawing.Point(55, 25);
            this.labelTitleChildForm.Name = "labelTitleChildForm";
            this.labelTitleChildForm.Size = new System.Drawing.Size(59, 28);
            this.labelTitleChildForm.TabIndex = 1;
            this.labelTitleChildForm.Text = "Home";
            // 
            // iconCurrentChildForm
            // 
            this.iconCurrentChildForm.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(3)))), ((int)(((byte)(29)))));
            this.iconCurrentChildForm.IconChar = FontAwesome.Sharp.IconChar.Home;
            this.iconCurrentChildForm.IconColor = System.Drawing.Color.White;
            this.iconCurrentChildForm.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.iconCurrentChildForm.Location = new System.Drawing.Point(17, 22);
            this.iconCurrentChildForm.Name = "iconCurrentChildForm";
            this.iconCurrentChildForm.Size = new System.Drawing.Size(32, 32);
            this.iconCurrentChildForm.TabIndex = 0;
            this.iconCurrentChildForm.TabStop = false;
            // 
            // panelChildForm
            // 
            this.panelChildForm.Controls.Add(this.pictureBox1);
            this.panelChildForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelChildForm.Location = new System.Drawing.Point(220, 75);
            this.panelChildForm.Name = "panelChildForm";
            this.panelChildForm.Size = new System.Drawing.Size(768, 615);
            this.panelChildForm.TabIndex = 2;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(297, 172);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(174, 220);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // AppForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(33)))));
            this.ClientSize = new System.Drawing.Size(988, 690);
            this.Controls.Add(this.panelChildForm);
            this.Controls.Add(this.panelTitleBar);
            this.Controls.Add(this.panelMenu);
            this.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "AppForm";
            this.Text = "Filtery";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.AppForm_FormClosed);
            this.panelMenu.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.buttonHome)).EndInit();
            this.panelTitleBar.ResumeLayout(false);
            this.panelTitleBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.iconCurrentChildForm)).EndInit();
            this.panelChildForm.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelMenu;
        private FontAwesome.Sharp.IconButton ImagesButton;
        private System.Windows.Forms.Panel panel1;
        private FontAwesome.Sharp.IconButton CameraButton;
        private System.Windows.Forms.PictureBox buttonHome;
        private System.Windows.Forms.Panel panelTitleBar;
        private FontAwesome.Sharp.IconPictureBox iconCurrentChildForm;
        private System.Windows.Forms.Label labelTitleChildForm;
        private System.Windows.Forms.Panel panelChildForm;
        private System.Windows.Forms.PictureBox pictureBox1;
        private FontAwesome.Sharp.IconButton VideosButton;
    }
}

