﻿namespace Filtery
{
    partial class VideosForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VideosForm));
            this.pictureBoxVisualizer = new System.Windows.Forms.PictureBox();
            this.panelSaveControls = new System.Windows.Forms.Panel();
            this.buttonPause = new System.Windows.Forms.Button();
            this.TrackBarVideo = new System.Windows.Forms.TrackBar();
            this.buttonStop = new System.Windows.Forms.Button();
            this.buttonPlay = new System.Windows.Forms.Button();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.panelFilters = new System.Windows.Forms.Panel();
            this.buttonReset = new System.Windows.Forms.Button();
            this.buttonMatrix = new System.Windows.Forms.Button();
            this.buttonBinary = new System.Windows.Forms.Button();
            this.buttonSepia = new System.Windows.Forms.Button();
            this.buttonGrayscale = new System.Windows.Forms.Button();
            this.buttonInvert = new System.Windows.Forms.Button();
            this.labelFilters = new System.Windows.Forms.Label();
            this.openFileDialogVideo = new System.Windows.Forms.OpenFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxVisualizer)).BeginInit();
            this.panelSaveControls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarVideo)).BeginInit();
            this.panelFilters.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBoxVisualizer
            // 
            this.pictureBoxVisualizer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(33)))));
            this.pictureBoxVisualizer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxVisualizer.Location = new System.Drawing.Point(37, 0);
            this.pictureBoxVisualizer.Name = "pictureBoxVisualizer";
            this.pictureBoxVisualizer.Size = new System.Drawing.Size(474, 445);
            this.pictureBoxVisualizer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxVisualizer.TabIndex = 15;
            this.pictureBoxVisualizer.TabStop = false;
            // 
            // panelSaveControls
            // 
            this.panelSaveControls.Controls.Add(this.buttonPause);
            this.panelSaveControls.Controls.Add(this.TrackBarVideo);
            this.panelSaveControls.Controls.Add(this.buttonStop);
            this.panelSaveControls.Controls.Add(this.buttonPlay);
            this.panelSaveControls.Controls.Add(this.buttonLoad);
            this.panelSaveControls.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelSaveControls.Location = new System.Drawing.Point(37, 445);
            this.panelSaveControls.Name = "panelSaveControls";
            this.panelSaveControls.Size = new System.Drawing.Size(474, 170);
            this.panelSaveControls.TabIndex = 14;
            // 
            // buttonPause
            // 
            this.buttonPause.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.buttonPause.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(8)))), ((int)(((byte)(20)))));
            this.buttonPause.Enabled = false;
            this.buttonPause.FlatAppearance.BorderSize = 0;
            this.buttonPause.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(16)))), ((int)(((byte)(6)))));
            this.buttonPause.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPause.ForeColor = System.Drawing.Color.White;
            this.buttonPause.Location = new System.Drawing.Point(25, 57);
            this.buttonPause.Name = "buttonPause";
            this.buttonPause.Size = new System.Drawing.Size(101, 37);
            this.buttonPause.TabIndex = 12;
            this.buttonPause.Text = "Pause";
            this.buttonPause.UseVisualStyleBackColor = false;
            this.buttonPause.Click += new System.EventHandler(this.buttonPause_Click);
            // 
            // TrackBarVideo
            // 
            this.TrackBarVideo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.TrackBarVideo.Location = new System.Drawing.Point(0, 0);
            this.TrackBarVideo.Name = "TrackBarVideo";
            this.TrackBarVideo.Size = new System.Drawing.Size(474, 45);
            this.TrackBarVideo.TabIndex = 11;
            this.TrackBarVideo.Scroll += new System.EventHandler(this.TrackBarVideo_Scroll);
            // 
            // buttonStop
            // 
            this.buttonStop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.buttonStop.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(8)))), ((int)(((byte)(20)))));
            this.buttonStop.Enabled = false;
            this.buttonStop.FlatAppearance.BorderSize = 0;
            this.buttonStop.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(16)))), ((int)(((byte)(6)))));
            this.buttonStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonStop.ForeColor = System.Drawing.Color.White;
            this.buttonStop.Location = new System.Drawing.Point(343, 57);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(101, 37);
            this.buttonStop.TabIndex = 10;
            this.buttonStop.Text = "Stop";
            this.buttonStop.UseVisualStyleBackColor = false;
            this.buttonStop.Click += new System.EventHandler(this.buttonStop_Click);
            // 
            // buttonPlay
            // 
            this.buttonPlay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.buttonPlay.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(8)))), ((int)(((byte)(20)))));
            this.buttonPlay.Enabled = false;
            this.buttonPlay.FlatAppearance.BorderSize = 0;
            this.buttonPlay.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(16)))), ((int)(((byte)(6)))));
            this.buttonPlay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPlay.ForeColor = System.Drawing.Color.White;
            this.buttonPlay.Location = new System.Drawing.Point(180, 57);
            this.buttonPlay.Name = "buttonPlay";
            this.buttonPlay.Size = new System.Drawing.Size(101, 37);
            this.buttonPlay.TabIndex = 8;
            this.buttonPlay.Text = "Play";
            this.buttonPlay.UseVisualStyleBackColor = false;
            this.buttonPlay.Click += new System.EventHandler(this.buttonPlay_Click);
            // 
            // buttonLoad
            // 
            this.buttonLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.buttonLoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(8)))), ((int)(((byte)(20)))));
            this.buttonLoad.FlatAppearance.BorderSize = 0;
            this.buttonLoad.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(16)))), ((int)(((byte)(6)))));
            this.buttonLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLoad.ForeColor = System.Drawing.Color.White;
            this.buttonLoad.Location = new System.Drawing.Point(180, 112);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(101, 37);
            this.buttonLoad.TabIndex = 6;
            this.buttonLoad.Text = "Load";
            this.buttonLoad.UseVisualStyleBackColor = false;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // panelFilters
            // 
            this.panelFilters.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(33)))));
            this.panelFilters.Controls.Add(this.buttonReset);
            this.panelFilters.Controls.Add(this.buttonMatrix);
            this.panelFilters.Controls.Add(this.buttonBinary);
            this.panelFilters.Controls.Add(this.buttonSepia);
            this.panelFilters.Controls.Add(this.buttonGrayscale);
            this.panelFilters.Controls.Add(this.buttonInvert);
            this.panelFilters.Controls.Add(this.labelFilters);
            this.panelFilters.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelFilters.Location = new System.Drawing.Point(511, 0);
            this.panelFilters.Name = "panelFilters";
            this.panelFilters.Size = new System.Drawing.Size(257, 615);
            this.panelFilters.TabIndex = 13;
            // 
            // buttonReset
            // 
            this.buttonReset.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(8)))), ((int)(((byte)(20)))));
            this.buttonReset.Enabled = false;
            this.buttonReset.FlatAppearance.BorderSize = 0;
            this.buttonReset.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(16)))), ((int)(((byte)(6)))));
            this.buttonReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonReset.ForeColor = System.Drawing.Color.White;
            this.buttonReset.Location = new System.Drawing.Point(37, 511);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(192, 51);
            this.buttonReset.TabIndex = 7;
            this.buttonReset.Text = "Reset";
            this.buttonReset.UseVisualStyleBackColor = false;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // buttonMatrix
            // 
            this.buttonMatrix.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonMatrix.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(8)))), ((int)(((byte)(20)))));
            this.buttonMatrix.Enabled = false;
            this.buttonMatrix.FlatAppearance.BorderSize = 0;
            this.buttonMatrix.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(16)))), ((int)(((byte)(6)))));
            this.buttonMatrix.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonMatrix.ForeColor = System.Drawing.Color.White;
            this.buttonMatrix.Location = new System.Drawing.Point(37, 390);
            this.buttonMatrix.Name = "buttonMatrix";
            this.buttonMatrix.Size = new System.Drawing.Size(192, 51);
            this.buttonMatrix.TabIndex = 5;
            this.buttonMatrix.Text = "Matrix";
            this.buttonMatrix.UseVisualStyleBackColor = false;
            this.buttonMatrix.Click += new System.EventHandler(this.buttonMatrix_Click);
            // 
            // buttonBinary
            // 
            this.buttonBinary.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBinary.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(8)))), ((int)(((byte)(20)))));
            this.buttonBinary.Enabled = false;
            this.buttonBinary.FlatAppearance.BorderSize = 0;
            this.buttonBinary.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(16)))), ((int)(((byte)(6)))));
            this.buttonBinary.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBinary.ForeColor = System.Drawing.Color.White;
            this.buttonBinary.Location = new System.Drawing.Point(37, 311);
            this.buttonBinary.Name = "buttonBinary";
            this.buttonBinary.Size = new System.Drawing.Size(192, 51);
            this.buttonBinary.TabIndex = 4;
            this.buttonBinary.Text = "Binary";
            this.buttonBinary.UseVisualStyleBackColor = false;
            this.buttonBinary.Click += new System.EventHandler(this.buttonBinary_Click);
            // 
            // buttonSepia
            // 
            this.buttonSepia.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSepia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(8)))), ((int)(((byte)(20)))));
            this.buttonSepia.Enabled = false;
            this.buttonSepia.FlatAppearance.BorderSize = 0;
            this.buttonSepia.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(16)))), ((int)(((byte)(6)))));
            this.buttonSepia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSepia.ForeColor = System.Drawing.Color.White;
            this.buttonSepia.Location = new System.Drawing.Point(37, 235);
            this.buttonSepia.Name = "buttonSepia";
            this.buttonSepia.Size = new System.Drawing.Size(192, 51);
            this.buttonSepia.TabIndex = 3;
            this.buttonSepia.Text = "Sepia";
            this.buttonSepia.UseVisualStyleBackColor = false;
            this.buttonSepia.Click += new System.EventHandler(this.buttonSepia_Click);
            // 
            // buttonGrayscale
            // 
            this.buttonGrayscale.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGrayscale.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(8)))), ((int)(((byte)(20)))));
            this.buttonGrayscale.Enabled = false;
            this.buttonGrayscale.FlatAppearance.BorderSize = 0;
            this.buttonGrayscale.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(16)))), ((int)(((byte)(6)))));
            this.buttonGrayscale.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGrayscale.ForeColor = System.Drawing.Color.White;
            this.buttonGrayscale.Location = new System.Drawing.Point(37, 161);
            this.buttonGrayscale.Name = "buttonGrayscale";
            this.buttonGrayscale.Size = new System.Drawing.Size(192, 51);
            this.buttonGrayscale.TabIndex = 2;
            this.buttonGrayscale.Text = "Grayscale";
            this.buttonGrayscale.UseVisualStyleBackColor = false;
            this.buttonGrayscale.Click += new System.EventHandler(this.buttonGrayscale_Click);
            // 
            // buttonInvert
            // 
            this.buttonInvert.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonInvert.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(8)))), ((int)(((byte)(20)))));
            this.buttonInvert.Enabled = false;
            this.buttonInvert.FlatAppearance.BorderSize = 0;
            this.buttonInvert.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(16)))), ((int)(((byte)(6)))));
            this.buttonInvert.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonInvert.ForeColor = System.Drawing.Color.White;
            this.buttonInvert.Location = new System.Drawing.Point(37, 87);
            this.buttonInvert.Name = "buttonInvert";
            this.buttonInvert.Size = new System.Drawing.Size(192, 51);
            this.buttonInvert.TabIndex = 1;
            this.buttonInvert.Text = "Invert";
            this.buttonInvert.UseVisualStyleBackColor = false;
            this.buttonInvert.Click += new System.EventHandler(this.buttonInvert_Click);
            // 
            // labelFilters
            // 
            this.labelFilters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelFilters.AutoSize = true;
            this.labelFilters.Font = new System.Drawing.Font("Poppins", 18F);
            this.labelFilters.ForeColor = System.Drawing.Color.White;
            this.labelFilters.Location = new System.Drawing.Point(90, 27);
            this.labelFilters.Name = "labelFilters";
            this.labelFilters.Size = new System.Drawing.Size(88, 42);
            this.labelFilters.TabIndex = 0;
            this.labelFilters.Text = "Filters";
            this.labelFilters.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // openFileDialogVideo
            // 
            this.openFileDialogVideo.Filter = "Videos|*.mp4;*.mov;*.flv";
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(37, 615);
            this.panel1.TabIndex = 16;
            // 
            // VideosForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(33)))));
            this.ClientSize = new System.Drawing.Size(768, 615);
            this.Controls.Add(this.pictureBoxVisualizer);
            this.Controls.Add(this.panelSaveControls);
            this.Controls.Add(this.panelFilters);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "VideosForm";
            this.Text = "Images";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxVisualizer)).EndInit();
            this.panelSaveControls.ResumeLayout(false);
            this.panelSaveControls.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrackBarVideo)).EndInit();
            this.panelFilters.ResumeLayout(false);
            this.panelFilters.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxVisualizer;
        private System.Windows.Forms.Panel panelSaveControls;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.Panel panelFilters;
        private System.Windows.Forms.Button buttonMatrix;
        private System.Windows.Forms.Button buttonBinary;
        private System.Windows.Forms.Button buttonSepia;
        private System.Windows.Forms.Button buttonGrayscale;
        private System.Windows.Forms.Button buttonInvert;
        private System.Windows.Forms.Label labelFilters;
        private System.Windows.Forms.OpenFileDialog openFileDialogVideo;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Button buttonPlay;
        private System.Windows.Forms.TrackBar TrackBarVideo;
        private System.Windows.Forms.Button buttonPause;
    }
}