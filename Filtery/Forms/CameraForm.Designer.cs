﻿namespace Filtery
{
    partial class CameraForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pictureBoxVisualizer = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.buttonTurnOnCamera = new FontAwesome.Sharp.IconPictureBox();
            this.comboBoxVideoDevice = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panelFilters = new System.Windows.Forms.Panel();
            this.labelStats = new System.Windows.Forms.Label();
            this.labelPeopleCounter = new System.Windows.Forms.Label();
            this.labelMovingCounter = new System.Windows.Forms.Label();
            this.labelMoving = new System.Windows.Forms.Label();
            this.labelPeople = new System.Windows.Forms.Label();
            this.timerStats = new System.Windows.Forms.Timer(this.components);
            this.checkBoxShowMotion = new System.Windows.Forms.CheckBox();
            this.label2 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxVisualizer)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonTurnOnCamera)).BeginInit();
            this.panelFilters.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBoxVisualizer
            // 
            this.pictureBoxVisualizer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(33)))));
            this.pictureBoxVisualizer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxVisualizer.Location = new System.Drawing.Point(0, 96);
            this.pictureBoxVisualizer.Name = "pictureBoxVisualizer";
            this.pictureBoxVisualizer.Size = new System.Drawing.Size(572, 519);
            this.pictureBoxVisualizer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxVisualizer.TabIndex = 12;
            this.pictureBoxVisualizer.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.buttonTurnOnCamera);
            this.panel1.Controls.Add(this.comboBoxVideoDevice);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(572, 96);
            this.panel1.TabIndex = 13;
            // 
            // buttonTurnOnCamera
            // 
            this.buttonTurnOnCamera.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.buttonTurnOnCamera.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(33)))));
            this.buttonTurnOnCamera.IconChar = FontAwesome.Sharp.IconChar.PowerOff;
            this.buttonTurnOnCamera.IconColor = System.Drawing.Color.White;
            this.buttonTurnOnCamera.IconFont = FontAwesome.Sharp.IconFont.Auto;
            this.buttonTurnOnCamera.Location = new System.Drawing.Point(482, 32);
            this.buttonTurnOnCamera.Name = "buttonTurnOnCamera";
            this.buttonTurnOnCamera.Size = new System.Drawing.Size(32, 32);
            this.buttonTurnOnCamera.TabIndex = 14;
            this.buttonTurnOnCamera.TabStop = false;
            this.buttonTurnOnCamera.Click += new System.EventHandler(this.iconTurnOnCamera_Click);
            // 
            // comboBoxVideoDevice
            // 
            this.comboBoxVideoDevice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.comboBoxVideoDevice.BackColor = System.Drawing.SystemColors.Window;
            this.comboBoxVideoDevice.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxVideoDevice.FormattingEnabled = true;
            this.comboBoxVideoDevice.Location = new System.Drawing.Point(175, 29);
            this.comboBoxVideoDevice.Name = "comboBoxVideoDevice";
            this.comboBoxVideoDevice.Size = new System.Drawing.Size(278, 36);
            this.comboBoxVideoDevice.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(45, 34);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(119, 28);
            this.label1.TabIndex = 8;
            this.label1.Text = "Video Device:";
            // 
            // panelFilters
            // 
            this.panelFilters.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(33)))));
            this.panelFilters.Controls.Add(this.label2);
            this.panelFilters.Controls.Add(this.checkBoxShowMotion);
            this.panelFilters.Controls.Add(this.labelStats);
            this.panelFilters.Controls.Add(this.labelPeopleCounter);
            this.panelFilters.Controls.Add(this.labelMovingCounter);
            this.panelFilters.Controls.Add(this.labelMoving);
            this.panelFilters.Controls.Add(this.labelPeople);
            this.panelFilters.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelFilters.Location = new System.Drawing.Point(572, 0);
            this.panelFilters.Name = "panelFilters";
            this.panelFilters.Size = new System.Drawing.Size(196, 615);
            this.panelFilters.TabIndex = 14;
            // 
            // labelStats
            // 
            this.labelStats.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelStats.AutoSize = true;
            this.labelStats.Font = new System.Drawing.Font("Poppins", 18F);
            this.labelStats.ForeColor = System.Drawing.Color.White;
            this.labelStats.Location = new System.Drawing.Point(65, 29);
            this.labelStats.Name = "labelStats";
            this.labelStats.Size = new System.Drawing.Size(79, 42);
            this.labelStats.TabIndex = 4;
            this.labelStats.Text = "Stats";
            this.labelStats.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelPeopleCounter
            // 
            this.labelPeopleCounter.AutoSize = true;
            this.labelPeopleCounter.ForeColor = System.Drawing.Color.White;
            this.labelPeopleCounter.Location = new System.Drawing.Point(90, 153);
            this.labelPeopleCounter.Name = "labelPeopleCounter";
            this.labelPeopleCounter.Size = new System.Drawing.Size(22, 28);
            this.labelPeopleCounter.TabIndex = 3;
            this.labelPeopleCounter.Text = "0";
            // 
            // labelMovingCounter
            // 
            this.labelMovingCounter.AutoSize = true;
            this.labelMovingCounter.ForeColor = System.Drawing.Color.White;
            this.labelMovingCounter.Location = new System.Drawing.Point(90, 261);
            this.labelMovingCounter.Name = "labelMovingCounter";
            this.labelMovingCounter.Size = new System.Drawing.Size(22, 28);
            this.labelMovingCounter.TabIndex = 2;
            this.labelMovingCounter.Text = "0";
            // 
            // labelMoving
            // 
            this.labelMoving.AutoSize = true;
            this.labelMoving.Font = new System.Drawing.Font("Poppins", 14F);
            this.labelMoving.ForeColor = System.Drawing.Color.White;
            this.labelMoving.Location = new System.Drawing.Point(60, 211);
            this.labelMoving.Name = "labelMoving";
            this.labelMoving.Size = new System.Drawing.Size(84, 34);
            this.labelMoving.TabIndex = 1;
            this.labelMoving.Text = "Moving";
            // 
            // labelPeople
            // 
            this.labelPeople.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelPeople.AutoSize = true;
            this.labelPeople.Font = new System.Drawing.Font("Poppins", 14F);
            this.labelPeople.ForeColor = System.Drawing.Color.White;
            this.labelPeople.Location = new System.Drawing.Point(64, 105);
            this.labelPeople.Name = "labelPeople";
            this.labelPeople.Size = new System.Drawing.Size(80, 34);
            this.labelPeople.TabIndex = 0;
            this.labelPeople.Text = "People";
            this.labelPeople.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timerStats
            // 
            this.timerStats.Enabled = true;
            this.timerStats.Interval = 10;
            this.timerStats.Tick += new System.EventHandler(this.timerStats_Tick);
            // 
            // checkBoxShowMotion
            // 
            this.checkBoxShowMotion.AutoEllipsis = true;
            this.checkBoxShowMotion.AutoSize = true;
            this.checkBoxShowMotion.FlatAppearance.BorderSize = 0;
            this.checkBoxShowMotion.ForeColor = System.Drawing.Color.White;
            this.checkBoxShowMotion.Location = new System.Drawing.Point(95, 345);
            this.checkBoxShowMotion.Name = "checkBoxShowMotion";
            this.checkBoxShowMotion.Size = new System.Drawing.Size(15, 14);
            this.checkBoxShowMotion.TabIndex = 5;
            this.checkBoxShowMotion.TabStop = false;
            this.checkBoxShowMotion.UseVisualStyleBackColor = true;
            this.checkBoxShowMotion.CheckedChanged += new System.EventHandler(this.checkBoxShowMotion_CheckedChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Poppins", 12F);
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(21, 310);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 28);
            this.label2.TabIndex = 6;
            this.label2.Text = "Motion Rectangles";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // CameraForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(33)))));
            this.ClientSize = new System.Drawing.Size(768, 615);
            this.Controls.Add(this.pictureBoxVisualizer);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelFilters);
            this.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "CameraForm";
            this.Text = "Camera";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CameraForm_FormClosed);
            this.Load += new System.EventHandler(this.CameraForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxVisualizer)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonTurnOnCamera)).EndInit();
            this.panelFilters.ResumeLayout(false);
            this.panelFilters.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxVisualizer;
        private System.Windows.Forms.Panel panel1;
        private FontAwesome.Sharp.IconPictureBox buttonTurnOnCamera;
        private System.Windows.Forms.ComboBox comboBoxVideoDevice;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panelFilters;
        private System.Windows.Forms.Label labelPeople;
        private System.Windows.Forms.Label labelMovingCounter;
        private System.Windows.Forms.Label labelMoving;
        private System.Windows.Forms.Label labelPeopleCounter;
        private System.Windows.Forms.Label labelStats;
        private System.Windows.Forms.Timer timerStats;
        private System.Windows.Forms.CheckBox checkBoxShowMotion;
        private System.Windows.Forms.Label label2;
    }
}