﻿namespace Filtery
{
    partial class ImagesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImagesForm));
            this.pictureBoxVisualizer = new System.Windows.Forms.PictureBox();
            this.panelSaveControls = new System.Windows.Forms.Panel();
            this.buttonSave = new System.Windows.Forms.Button();
            this.buttonLoad = new System.Windows.Forms.Button();
            this.panelFilters = new System.Windows.Forms.Panel();
            this.buttonColorGradient = new System.Windows.Forms.Button();
            this.buttonBinary = new System.Windows.Forms.Button();
            this.buttonPixelate = new System.Windows.Forms.Button();
            this.buttonGrayscale = new System.Windows.Forms.Button();
            this.buttonInvert = new System.Windows.Forms.Button();
            this.labelFilters = new System.Windows.Forms.Label();
            this.openFileDialogImage = new System.Windows.Forms.OpenFileDialog();
            this.panel1 = new System.Windows.Forms.Panel();
            this.saveFileDialogImage = new System.Windows.Forms.SaveFileDialog();
            this.buttonReset = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxVisualizer)).BeginInit();
            this.panelSaveControls.SuspendLayout();
            this.panelFilters.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBoxVisualizer
            // 
            this.pictureBoxVisualizer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(33)))));
            this.pictureBoxVisualizer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxVisualizer.Location = new System.Drawing.Point(37, 0);
            this.pictureBoxVisualizer.Name = "pictureBoxVisualizer";
            this.pictureBoxVisualizer.Size = new System.Drawing.Size(474, 465);
            this.pictureBoxVisualizer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxVisualizer.TabIndex = 15;
            this.pictureBoxVisualizer.TabStop = false;
            // 
            // panelSaveControls
            // 
            this.panelSaveControls.Controls.Add(this.buttonSave);
            this.panelSaveControls.Controls.Add(this.buttonLoad);
            this.panelSaveControls.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelSaveControls.Location = new System.Drawing.Point(37, 465);
            this.panelSaveControls.Name = "panelSaveControls";
            this.panelSaveControls.Size = new System.Drawing.Size(474, 150);
            this.panelSaveControls.TabIndex = 14;
            // 
            // buttonSave
            // 
            this.buttonSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.buttonSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(8)))), ((int)(((byte)(20)))));
            this.buttonSave.Enabled = false;
            this.buttonSave.FlatAppearance.BorderSize = 0;
            this.buttonSave.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(16)))), ((int)(((byte)(6)))));
            this.buttonSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonSave.ForeColor = System.Drawing.Color.White;
            this.buttonSave.Location = new System.Drawing.Point(289, 42);
            this.buttonSave.Name = "buttonSave";
            this.buttonSave.Size = new System.Drawing.Size(155, 59);
            this.buttonSave.TabIndex = 7;
            this.buttonSave.Text = "Save";
            this.buttonSave.UseVisualStyleBackColor = false;
            this.buttonSave.Click += new System.EventHandler(this.buttonSave_Click);
            // 
            // buttonLoad
            // 
            this.buttonLoad.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.buttonLoad.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(8)))), ((int)(((byte)(20)))));
            this.buttonLoad.FlatAppearance.BorderSize = 0;
            this.buttonLoad.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(16)))), ((int)(((byte)(6)))));
            this.buttonLoad.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonLoad.ForeColor = System.Drawing.Color.White;
            this.buttonLoad.Location = new System.Drawing.Point(23, 42);
            this.buttonLoad.Name = "buttonLoad";
            this.buttonLoad.Size = new System.Drawing.Size(155, 59);
            this.buttonLoad.TabIndex = 6;
            this.buttonLoad.Text = "Load";
            this.buttonLoad.UseVisualStyleBackColor = false;
            this.buttonLoad.Click += new System.EventHandler(this.buttonLoad_Click);
            // 
            // panelFilters
            // 
            this.panelFilters.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(33)))));
            this.panelFilters.Controls.Add(this.buttonReset);
            this.panelFilters.Controls.Add(this.buttonColorGradient);
            this.panelFilters.Controls.Add(this.buttonBinary);
            this.panelFilters.Controls.Add(this.buttonPixelate);
            this.panelFilters.Controls.Add(this.buttonGrayscale);
            this.panelFilters.Controls.Add(this.buttonInvert);
            this.panelFilters.Controls.Add(this.labelFilters);
            this.panelFilters.Dock = System.Windows.Forms.DockStyle.Right;
            this.panelFilters.Location = new System.Drawing.Point(511, 0);
            this.panelFilters.Name = "panelFilters";
            this.panelFilters.Size = new System.Drawing.Size(257, 615);
            this.panelFilters.TabIndex = 13;
            // 
            // buttonColorGradient
            // 
            this.buttonColorGradient.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonColorGradient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(8)))), ((int)(((byte)(20)))));
            this.buttonColorGradient.Enabled = false;
            this.buttonColorGradient.FlatAppearance.BorderSize = 0;
            this.buttonColorGradient.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(16)))), ((int)(((byte)(6)))));
            this.buttonColorGradient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonColorGradient.ForeColor = System.Drawing.Color.White;
            this.buttonColorGradient.Location = new System.Drawing.Point(37, 390);
            this.buttonColorGradient.Name = "buttonColorGradient";
            this.buttonColorGradient.Size = new System.Drawing.Size(192, 51);
            this.buttonColorGradient.TabIndex = 5;
            this.buttonColorGradient.Text = "Color Gradient";
            this.buttonColorGradient.UseVisualStyleBackColor = false;
            this.buttonColorGradient.Click += new System.EventHandler(this.buttonColorGradient_Click);
            // 
            // buttonBinary
            // 
            this.buttonBinary.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonBinary.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(8)))), ((int)(((byte)(20)))));
            this.buttonBinary.Enabled = false;
            this.buttonBinary.FlatAppearance.BorderSize = 0;
            this.buttonBinary.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(16)))), ((int)(((byte)(6)))));
            this.buttonBinary.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonBinary.ForeColor = System.Drawing.Color.White;
            this.buttonBinary.Location = new System.Drawing.Point(37, 311);
            this.buttonBinary.Name = "buttonBinary";
            this.buttonBinary.Size = new System.Drawing.Size(192, 51);
            this.buttonBinary.TabIndex = 4;
            this.buttonBinary.Text = "Binary";
            this.buttonBinary.UseVisualStyleBackColor = false;
            this.buttonBinary.Click += new System.EventHandler(this.buttonBinary_Click);
            // 
            // buttonPixelate
            // 
            this.buttonPixelate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonPixelate.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(8)))), ((int)(((byte)(20)))));
            this.buttonPixelate.Enabled = false;
            this.buttonPixelate.FlatAppearance.BorderSize = 0;
            this.buttonPixelate.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(16)))), ((int)(((byte)(6)))));
            this.buttonPixelate.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonPixelate.ForeColor = System.Drawing.Color.White;
            this.buttonPixelate.Location = new System.Drawing.Point(37, 235);
            this.buttonPixelate.Name = "buttonPixelate";
            this.buttonPixelate.Size = new System.Drawing.Size(192, 51);
            this.buttonPixelate.TabIndex = 3;
            this.buttonPixelate.Text = "Pixelate";
            this.buttonPixelate.UseVisualStyleBackColor = false;
            this.buttonPixelate.Click += new System.EventHandler(this.buttonPixelate_Click);
            // 
            // buttonGrayscale
            // 
            this.buttonGrayscale.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonGrayscale.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(8)))), ((int)(((byte)(20)))));
            this.buttonGrayscale.Enabled = false;
            this.buttonGrayscale.FlatAppearance.BorderSize = 0;
            this.buttonGrayscale.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(16)))), ((int)(((byte)(6)))));
            this.buttonGrayscale.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonGrayscale.ForeColor = System.Drawing.Color.White;
            this.buttonGrayscale.Location = new System.Drawing.Point(37, 161);
            this.buttonGrayscale.Name = "buttonGrayscale";
            this.buttonGrayscale.Size = new System.Drawing.Size(192, 51);
            this.buttonGrayscale.TabIndex = 2;
            this.buttonGrayscale.Text = "Grayscale";
            this.buttonGrayscale.UseVisualStyleBackColor = false;
            this.buttonGrayscale.Click += new System.EventHandler(this.buttonGrayscale_Click);
            // 
            // buttonInvert
            // 
            this.buttonInvert.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonInvert.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(8)))), ((int)(((byte)(20)))));
            this.buttonInvert.Enabled = false;
            this.buttonInvert.FlatAppearance.BorderSize = 0;
            this.buttonInvert.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(16)))), ((int)(((byte)(6)))));
            this.buttonInvert.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonInvert.ForeColor = System.Drawing.Color.White;
            this.buttonInvert.Location = new System.Drawing.Point(37, 87);
            this.buttonInvert.Name = "buttonInvert";
            this.buttonInvert.Size = new System.Drawing.Size(192, 51);
            this.buttonInvert.TabIndex = 1;
            this.buttonInvert.Text = "Invert";
            this.buttonInvert.UseVisualStyleBackColor = false;
            this.buttonInvert.Click += new System.EventHandler(this.buttonInvert_Click);
            // 
            // labelFilters
            // 
            this.labelFilters.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelFilters.AutoSize = true;
            this.labelFilters.Font = new System.Drawing.Font("Poppins", 18F);
            this.labelFilters.ForeColor = System.Drawing.Color.White;
            this.labelFilters.Location = new System.Drawing.Point(90, 27);
            this.labelFilters.Name = "labelFilters";
            this.labelFilters.Size = new System.Drawing.Size(88, 42);
            this.labelFilters.TabIndex = 0;
            this.labelFilters.Text = "Filters";
            this.labelFilters.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // openFileDialogImage
            // 
            this.openFileDialogImage.Filter = "Images|*.png;*.bmp;*.jpg";
            // 
            // panel1
            // 
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(37, 615);
            this.panel1.TabIndex = 16;
            // 
            // saveFileDialogImage
            // 
            this.saveFileDialogImage.Filter = "Images|*.png;*.bmp;*.jpg";
            // 
            // buttonReset
            // 
            this.buttonReset.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonReset.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(74)))), ((int)(((byte)(8)))), ((int)(((byte)(20)))));
            this.buttonReset.Enabled = false;
            this.buttonReset.FlatAppearance.BorderSize = 0;
            this.buttonReset.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(117)))), ((int)(((byte)(16)))), ((int)(((byte)(6)))));
            this.buttonReset.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonReset.ForeColor = System.Drawing.Color.White;
            this.buttonReset.Location = new System.Drawing.Point(37, 511);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(192, 51);
            this.buttonReset.TabIndex = 7;
            this.buttonReset.Text = "Reset";
            this.buttonReset.UseVisualStyleBackColor = false;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // ImagesForm
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(31)))), ((int)(((byte)(0)))), ((int)(((byte)(33)))));
            this.ClientSize = new System.Drawing.Size(768, 615);
            this.Controls.Add(this.pictureBoxVisualizer);
            this.Controls.Add(this.panelSaveControls);
            this.Controls.Add(this.panelFilters);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Poppins", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ImagesForm";
            this.Text = "Images";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxVisualizer)).EndInit();
            this.panelSaveControls.ResumeLayout(false);
            this.panelFilters.ResumeLayout(false);
            this.panelFilters.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBoxVisualizer;
        private System.Windows.Forms.Panel panelSaveControls;
        private System.Windows.Forms.Button buttonSave;
        private System.Windows.Forms.Button buttonLoad;
        private System.Windows.Forms.Panel panelFilters;
        private System.Windows.Forms.Button buttonColorGradient;
        private System.Windows.Forms.Button buttonBinary;
        private System.Windows.Forms.Button buttonPixelate;
        private System.Windows.Forms.Button buttonGrayscale;
        private System.Windows.Forms.Button buttonInvert;
        private System.Windows.Forms.Label labelFilters;
        private System.Windows.Forms.OpenFileDialog openFileDialogImage;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.SaveFileDialog saveFileDialogImage;
        private System.Windows.Forms.Button buttonReset;
    }
}