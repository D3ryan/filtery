﻿using Filtery.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Filtery
{
    public partial class ImagesForm : Form
    {
        Image originalFile;

        public ImagesForm()
        {
            InitializeComponent();
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            LoadImageIntoPictureBox();
        }

        private void buttonInvert_Click(object sender, EventArgs e)
        {
            pictureBoxVisualizer.Image = Filters.Invert(pictureBoxVisualizer.Image);
        }

        private void buttonGrayscale_Click(object sender, EventArgs e)
        {
            pictureBoxVisualizer.Image = Filters.Grayscale(pictureBoxVisualizer.Image);
        }

        private void buttonPixelate_Click(object sender, EventArgs e)
        {
            pictureBoxVisualizer.Image = Filters.Pixelate(pictureBoxVisualizer.Image);
        }

        private void buttonBinary_Click(object sender, EventArgs e)
        {
            pictureBoxVisualizer.Image = Filters.Binary(pictureBoxVisualizer.Image);
        }

        private void buttonColorGradient_Click(object sender, EventArgs e)
        {
            pictureBoxVisualizer.Image = Filters.ColorGradient(pictureBoxVisualizer.Image);
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            pictureBoxVisualizer.Image = originalFile;
        }

        private void buttonSave_Click(object sender, EventArgs e)
        {
            SaveImage();
        }

        private void LoadImageIntoPictureBox()
        {
            if(openFileDialogImage.ShowDialog() == DialogResult.OK)
            {
                originalFile = Image.FromFile(openFileDialogImage.FileName);
                pictureBoxVisualizer.Image = originalFile;
                EnableButtons();
            }
        }

        private void SaveImage()
        {

            if (saveFileDialogImage.ShowDialog() == DialogResult.OK)
            {

                ImageFormat format = ImageFormat.Png;
                string ext = Path.GetExtension(saveFileDialogImage.FileName);
                switch (ext)
                {
                    case ".jpg":
                        format = ImageFormat.Jpeg;
                        break;
                    case ".bmp":
                        format = ImageFormat.Bmp;
                        break;
                }
                pictureBoxVisualizer.Image.Save(saveFileDialogImage.FileName, format);
            }
        }

        private void EnableButtons()
        {
            buttonInvert.Enabled = true;
            buttonGrayscale.Enabled = true;
            buttonPixelate.Enabled = true;
            buttonBinary.Enabled = true;
            buttonColorGradient.Enabled = true;
            buttonSave.Enabled = true;
            buttonReset.Enabled = true;
        }


    }
}
