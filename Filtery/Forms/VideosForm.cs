﻿using Filtery.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;

namespace Filtery
{
    enum FilterType
    {
        Original,
        Invert,
        Grayscale,
        Sepia,
        Binary,
        Matrix
    }

    public partial class VideosForm : Form
    {

        VideoCapture videoCapture;
        bool isPlaying = false;
        int totalFrames;
        int currentFrameNumber;
        int fps;
        Mat currentFrame;
        FilterType filterType;

        public VideosForm()
        {
            InitializeComponent();
        }

        private void buttonLoad_Click(object sender, EventArgs e)
        {
            LoadVideoIntoPictureBox();
        }

        private void buttonInvert_Click(object sender, EventArgs e)
        {
            filterType = FilterType.Invert;
        }

        private void buttonGrayscale_Click(object sender, EventArgs e)
        {
            filterType = FilterType.Grayscale;
        }

        private void buttonSepia_Click(object sender, EventArgs e)
        {
            filterType = FilterType.Sepia;
        }

        private void buttonBinary_Click(object sender, EventArgs e)
        {
            filterType = FilterType.Binary;
        }

        private void buttonMatrix_Click(object sender, EventArgs e)
        {
            filterType = FilterType.Matrix;
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            filterType = FilterType.Original;
        }

        private void buttonPause_Click(object sender, EventArgs e)
        {
            isPlaying = false;
        }
        private void buttonStop_Click(object sender, EventArgs e)
        {
            isPlaying=false;
            currentFrameNumber = 0;
            TrackBarVideo.Value = 0;
            pictureBoxVisualizer.Image = null;
            pictureBoxVisualizer.Invalidate();
            buttonLoad.Enabled = true;
        }

        private void buttonPlay_Click(object sender, EventArgs e)
        {
            if(videoCapture != null)
            {
                isPlaying = true;
                PlayVideo();
            }
            else
            {
                isPlaying=false;
            }
        }

        private void LoadVideoIntoPictureBox()
        {
            if(openFileDialogVideo.ShowDialog() == DialogResult.OK)
            {
                videoCapture = new VideoCapture(openFileDialogVideo.FileName);
                totalFrames = Convert.ToInt32(videoCapture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.FrameCount));
                fps = Convert.ToInt32(videoCapture.GetCaptureProperty(Emgu.CV.CvEnum.CapProp.Fps));
                currentFrame = new Mat();
                currentFrameNumber = 0;

                setVideoTrackbarSettings(totalFrames);

                PlayVideo();

                EnableButtons();

                buttonLoad.Enabled = false;
            }
        }

        private async void PlayVideo()
        {
            if(videoCapture != null)
            {
                isPlaying = true;
                Bitmap newBmp;

                try
                {
                    while (isPlaying && currentFrameNumber < totalFrames)
                    {
                        
                        videoCapture.Read(currentFrame);

                        switch (filterType)
                        {
                            case FilterType.Original:
                                newBmp = currentFrame.Bitmap;
                                break;
                            case FilterType.Invert:
                                newBmp = VideoFilters.Invert(currentFrame.Bitmap);
                                break;
                            case FilterType.Grayscale:
                                newBmp = VideoFilters.Grayscale(currentFrame.Bitmap);
                                break;
                            case FilterType.Sepia:
                                newBmp = VideoFilters.Sepia(currentFrame.Bitmap);
                                break;
                            case FilterType.Binary:
                                newBmp = VideoFilters.Binary(currentFrame.Bitmap);
                                break;
                            case FilterType.Matrix:
                                newBmp = VideoFilters.Matrix(currentFrame.Bitmap);
                                break;
                            default:
                                newBmp = currentFrame.Bitmap;
                                break;
                        }

                        pictureBoxVisualizer.Image = newBmp;

                        TrackBarVideo.Value = currentFrameNumber;
                        currentFrameNumber++;

                        await Task.Delay(1000 / fps);

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }

        }

        private void setVideoTrackbarSettings(int totalFrames)
        {
            TrackBarVideo.Minimum = 0;
            TrackBarVideo.Maximum = totalFrames - 1;
            TrackBarVideo.Value = 0;
        }

        private void EnableButtons()
        {
            buttonInvert.Enabled = true;
            buttonGrayscale.Enabled = true;
            buttonSepia.Enabled = true;
            buttonBinary.Enabled = true;
            buttonMatrix.Enabled = true;
            buttonReset.Enabled = true;
            buttonPlay.Enabled = true;
            buttonPause.Enabled = true;
            buttonStop.Enabled = true;
        }

        private void TrackBarVideo_Scroll(object sender, EventArgs e)
        {
            if(videoCapture != null)
            {
                currentFrameNumber = TrackBarVideo.Value;
                videoCapture.SetCaptureProperty(Emgu.CV.CvEnum.CapProp.PosFrames, currentFrameNumber);
            }
        }
    }
}
