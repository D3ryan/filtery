﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Video.DirectShow;
using AForge.Video;
using AForge.Vision.Motion;
using Emgu.CV;
using Emgu.CV.Structure;

namespace Filtery
{
    public partial class CameraForm : Form
    {
        private FilterInfoCollection videoCaptureDevices;
        private VideoCaptureDevice selectedVideoCaptureDevice;
        private Bitmap frame;
        private Image<Bgr, byte> grayImage;
        private Rectangle[] faces;
        private List<Color> colors = new List<Color>();
        private SimpleBackgroundModelingDetector detector = new SimpleBackgroundModelingDetector();
        private BlobCountingObjectsProcessing processor = new BlobCountingObjectsProcessing();
        private MotionDetector motionDetector;
        static readonly CascadeClassifier cascadeClassifier = new CascadeClassifier("../../Utils/haarcascade_frontalface_alt_tree.xml");

        public CameraForm()
        {
            InitializeComponent();
        }

        private void CameraForm_Load(object sender, EventArgs e)
        {
            LoadVideoCaptureDeviceNames();
            InstantiateMotionDetector();
        }

        private void InstantiateMotionDetector()
        {
            processor.MinObjectsWidth = 150;
            processor.MinObjectsHeight = 150;
            processor.HighlightMotionRegions = false;
            motionDetector = new MotionDetector(detector, processor);
        }

        private void CameraForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            UnloadVideoCaptureDevice();
        }

        private void iconTurnOnCamera_Click(object sender, EventArgs e)
        {
            UnloadVideoCaptureDevice();
            string videoCaptureDeviceName = videoCaptureDevices[comboBoxVideoDevice.SelectedIndex].MonikerString;
            selectedVideoCaptureDevice = new VideoCaptureDevice(videoCaptureDeviceName);
            selectedVideoCaptureDevice.NewFrame += new NewFrameEventHandler(Capture);
            selectedVideoCaptureDevice.Start();
        }

        public void LoadVideoCaptureDeviceNames()
        {
            videoCaptureDevices = new FilterInfoCollection(FilterCategory.VideoInputDevice);

            if(videoCaptureDevices.Count > 0)
            {
                for (int i = 0; i < videoCaptureDevices.Count; i++) comboBoxVideoDevice.Items.Add(videoCaptureDevices[i].Name.ToString());
                comboBoxVideoDevice.Text = videoCaptureDevices[0].Name.ToString();
            }
        }

        public void UnloadVideoCaptureDevice()
        {
            if(selectedVideoCaptureDevice != null && selectedVideoCaptureDevice.IsRunning)
            {
                selectedVideoCaptureDevice.SignalToStop();
                selectedVideoCaptureDevice = null;
            }
        }

        private new void Capture(object sender, NewFrameEventArgs e)
        {
            try
            {
                frame = (Bitmap)e.Frame.Clone();

                motionDetector.ProcessFrame(frame);

                grayImage = new Image<Bgr, byte>(frame);

                faces = cascadeClassifier.DetectMultiScale(grayImage, 1.2, 1);

                int difference = faces.Length - colors.Count;

                for (int i = 0; i < difference; i++)
                {
                    Random random = new Random();
                    colors.Add(Color.FromArgb(random.Next(256), random.Next(256), random.Next(256)));
                }


                for (int i = 0; i < faces.Length; i++)
                {
                    Font font = new Font("Poppins", 25);

                    StringFormat format = new StringFormat();
                    format.FormatFlags = StringFormatFlags.DirectionRightToLeft;

                    using (Graphics graphics = Graphics.FromImage(frame))
                    {
                        using (Pen pen = new Pen(colors[i], 3.5f))
                        {
                            SolidBrush brush = new SolidBrush(colors[i]);
                            graphics.DrawRectangle(pen, faces[i]);
                            graphics.DrawString((i + 1).ToString(), font, brush, faces[i].X, faces[i].Y - 10, format);
                        }
                    }

                }

                pictureBoxVisualizer.Image = frame;
            }
            catch (Exception exception)
            {
                MessageBox.Show("Ha ocurrido un error, lamentamos las molestias\n\nError: " + exception.Message, "Error",MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            

        }

        private void timerStats_Tick(object sender, EventArgs e)
        {
            if(faces != null) labelPeopleCounter.Text = faces.Length.ToString();
            labelMovingCounter.Text = processor.ObjectsCount.ToString();
        }

        private void checkBoxShowMotion_CheckedChanged(object sender, EventArgs e)
        {
            processor.HighlightMotionRegions = !processor.HighlightMotionRegions;
        }
    }
}
